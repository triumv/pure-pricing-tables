`/** @jsx React.DOM */`
require('pure/base.css')
require('pure/menus.css')
require('pure/buttons.css')
require('./grid.css')
require('./style.scss')

Component = React.createClass
  render: ->
    `<div className="pure-pricing-tables">
      <div className="l-content">
        <div className="information pure-g">
          <div className="pure-u-1 pure-u-med-1-2">
            <div className="l-box">
              <h3 className="information-head">About Us</h3>
              <p>
                Every business requires an online presence.  That presence requires care and attention to both create and manage.  We are devoted to making sites that nourish your online presence.

                BFS Consulting was started by three web developers who wanted to change the way people think and interact with websites.  We provide an experience for our clients as well as their customers that is unique and intuitive.
              </p>
            </div>
          </div>
          <div className="pure-u-1 pure-u-med-1-2">
            <div className="l-box">
              <h3 className="information-head">What We Do</h3>
              <p>
                We provide a full coverage white glove service that includes design, development, management, and hosting for reactive websites that make sense in both mobile and desktop.
              </p>
            </div>
          </div>
          <div className="pure-u-1 pure-u-med-1-2">
            <div className="l-box">
              <h3 className="information-head">Why Us?</h3>
              <p>
              
              </p>
            </div>
          </div>
          <div className="pure-u-1 pure-u-med-1-2">
            <div className="l-box">
              <h3 className="information-head">Contract Us</h3>
              <p>
                Interested in creating an unforgettable and unique web presence? Contact us for a custom quote.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>`

module.exports = Component
